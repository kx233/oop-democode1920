package uk.ac.cam.acr31.oop.democode1920.lecture2;

import static com.google.common.truth.Truth.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class EvenTest {

  @Test
  public void isOdd_returnsFalse_whenEven() {
    // ARRANGE
    int x = 4;

    // ACT
    boolean even = Even.isOdd(x);

    // ASSERT
    assertThat(even).isFalse();
  }

  @Test
  public void isOdd_returnsFalse_whenNegativeEven() {
    // ARRANGE
    int x = -4;

    // ACT
    boolean even = Even.isOdd(x);

    // ASSERT
    assertThat(even).isFalse();
  }

  @Test
  public void isOdd_returnsTrue_whenOdd() {
    // ARRANGE
    int x = 5;

    // ACT
    boolean even = Even.isOdd(x);

    // ASSERT
    assertThat(even).isTrue();
  }

  @Test
  public void isOdd_returnsTrue_whenNegativeOdd() {
    // ARRANGE
    int x = -5;

    // ACT
    boolean even = Even.isOdd(x);

    // ASSERT
    assertThat(even).isTrue();
  }
}
