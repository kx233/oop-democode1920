package uk.ac.cam.acr31.oop.democode1920.lecture12;

class MotorController {

  void turnFast() {}

  void turnSlow() {}

  void stop() {}
}
