package uk.ac.cam.acr31.oop.democode1920.lecture6;

import java.util.ArrayList;
import java.util.List;

public class Substitution {

  private static void airQuote(Object o) {
    System.out.println("'" + o + "'");
  }

  private static void printSize(List<Integer> stuff) {
    System.out.println(stuff.size());
  }

  public static void main(String[] args) {
    airQuote("Java is cool");
    airQuote(new Object());
    printSize(new ArrayList<Integer>());
    // doesn't compile - printSize(new HashSet<Integer>());
  }
}
