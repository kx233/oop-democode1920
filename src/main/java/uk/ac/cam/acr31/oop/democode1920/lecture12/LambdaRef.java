package uk.ac.cam.acr31.oop.democode1920.lecture12;

import java.util.List;
import java.util.stream.Collectors;

class Doubler {

  int dbl(int x) {
    return x * 2;
  }
}

class StaticOrNot {

  int x;

  int dbl() {
    return this.x * 2;
  }

  static int dbl2(StaticOrNot thiss) {
    return thiss.x * 2;
  }
}

public class LambdaRef {

  public static void main(String[] args) {
    List<Integer> ints = List.of(1, 2, 3, 4, 5, 6);
    Doubler dd = new Doubler();
    List<Integer> doubled = ints.stream().map(dd::dbl).collect(Collectors.toList());

    List<String> strings = List.of("andy", "is", "here");
    List<String> upper = strings.stream().map(String::toUpperCase).collect(Collectors.toList());
  }
}
