package uk.ac.cam.acr31.oop.democode1920.lecture11;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Streams {

  private int last = 0;

  void stateIsBad() {
    int sum =
        IntStream.range(0, 1000)
            .parallel()
            .map(
                v -> {
                  last = v;
                  return v;
                })
            .sum();
    System.out.println(last);
  }

  public static void main(String[] args) {

    List<College> colleges = College.colleges();

    // List of names
    List<String> names = new ArrayList<>();
    for (College college : colleges) {
      names.add(college.getName());
    }
    List<String> streamNames = colleges.stream().map(College::getName).collect(toList());

    // Sorted alphabetically
    List<String> sortedName = colleges.stream().map(College::getName).sorted().collect(toList());

    // Sorted by founding year
    Comparator<College> byFoundingYear = Comparator.comparing(College::getFoundingYear).reversed();
    List<String> ageOrdered =
        colleges.stream().sorted(byFoundingYear).map(College::getName).collect(toList());

    // List of ages
    List<Integer> ages = colleges.stream().map(c -> 2019 - c.getFoundingYear()).collect(toList());

    // Average age
    double averageAge =
        colleges.stream().mapToInt(c -> 2019 - c.getFoundingYear()).average().orElseThrow();

    // Map of names to founding year
    Map<String, Integer> lookup =
        colleges.stream().collect(toMap(College::getName, College::getFoundingYear));

    // don't do this
    Stream<College> streamInAVariable = colleges.stream();

    // State is bad
    Streams s = new Streams();
    s.stateIsBad();
  }
}
