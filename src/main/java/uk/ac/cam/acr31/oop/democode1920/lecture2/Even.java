package uk.ac.cam.acr31.oop.democode1920.lecture2;

class Even {

  static boolean isOdd(int x) {
    // remember that mod negative numbers is negative
    return x % 2 != 0;
  }
}
