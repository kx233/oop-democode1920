package uk.ac.cam.acr31.oop.democode1920.lecture7;

public class Finalizer {

  @Override
  protected void finalize() throws Throwable {
    super.finalize();
    System.out.println("I'm free!");
  }

  public static void main(String[] args) {
    for (int i = 0; i < 1000000; i++) {
      Finalizer f = new Finalizer();
    }
  }
}
