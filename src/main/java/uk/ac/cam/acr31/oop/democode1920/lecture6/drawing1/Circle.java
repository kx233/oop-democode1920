package uk.ac.cam.acr31.oop.democode1920.lecture6.drawing1;

public class Circle {

  private final int x;

  public Circle(int x) {
    this.x = x;
  }

  void draw(AsciiImage asciiImage) {
    asciiImage.draw(x, 'o');
  }
}
