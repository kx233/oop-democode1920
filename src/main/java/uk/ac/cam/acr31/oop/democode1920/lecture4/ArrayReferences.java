package uk.ac.cam.acr31.oop.democode1920.lecture4;

import java.util.Arrays;

public class ArrayReferences {

  private static int a(int[] x) {
    return x[0] + 1;
  }

  private static void b(int[] x) {
    x[0] = x[0] + 1;
  }

  public static void main(String[] args) {
    int[] x = new int[] {1};
    int ax = a(x);
    b(x);
    System.out.printf("%d, %s%n", ax, Arrays.toString(x));
  }
}
