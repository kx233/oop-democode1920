package uk.ac.cam.acr31.oop.democode1920.lecture7;

import java.io.Closeable;

public class TryWithResources implements Closeable {

  TryWithResources() {
    System.out.println("Acquire");
  }

  public static void main(String[] args) {
    System.out.println("start");
    try (TryWithResources t = new TryWithResources()) {
      System.out.println("execute");
      // object exists

    }
    System.out.println("done");
    // object still exists - but close will have been called

  }

  @Override
  public void close() {
    System.out.println("release");
  }
}
