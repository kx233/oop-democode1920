package uk.ac.cam.acr31.oop.democode1920.lecture12;

/**
 * Model a desk fan with three settings: off, slow and fast.
 *
 * <p>The fan has one button to click through these three settings.
 *
 * <p>The two methods on this class are called by the firmware of the fan. {@code click} is called
 * whenever someone presses the button to change setting. {@code update} is called whenever the
 * motor is ready to change speed.
 */
public class FanSpeedWithStatePattern {

  interface FanState {
    void update(MotorController motorController);

    void click();
  }

  class Stop implements FanState {
    @Override
    public void update(MotorController motorController) {
      motorController.stop();
    }

    @Override
    public void click() {
      state = new Slow();
    }
  }

  class Slow implements FanState {

    @Override
    public void update(MotorController motorController) {
      motorController.turnSlow();
    }

    @Override
    public void click() {
      state = new Fast();
    }
  }

  class Fast implements FanState {

    @Override
    public void update(MotorController motorController) {
      motorController.turnFast();
    }

    @Override
    public void click() {
      state = new Stop();
    }
  }

  private FanState state = new Stop();

  /** Set the motor turning at the correct speed. */
  void update(MotorController motorController) {
    state.update(motorController);
  }

  /** Respond to a button click to change between settings. */
  void click() {
    state.click();
  }
}
