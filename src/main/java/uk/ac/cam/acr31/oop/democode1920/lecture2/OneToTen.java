package uk.ac.cam.acr31.oop.democode1920.lecture2;

import java.util.List;
import java.util.stream.IntStream;

public class OneToTen {

  static void whileLoop() {
    int x = 1;
    while (x < 11) {
      System.out.println(x++);
    }
  }

  static void forLoop() {
    for (int i = 1; i <= 10; i++) {
      System.out.println(i);
    }
  }

  static void loadsOfIfs() {
    System.out.println(1);
    System.out.println(2);
    // ...
  }

  static void recursive(int i) {
    if (i == 11) {
      return;
    }
    System.out.println(i);
    recursive(i + 1);
  }

  static void recursive() {
    recursive(1);
  }

  static void doWhileLoop() {
    int i = 1;
    do {
      System.out.println(i++);
    } while (i < 11);
  }

  static void intStream() {
    IntStream.range(1, 11).forEach(System.out::println);
  }

  static void forEach() {
    List<Integer> list = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
    for (int i : list) {
      System.out.println(i);
    }
  }

  static void doWhileBreak() {
    int i = 1;
    while (true) {
      System.out.println(i++);
      if (i == 11) {
        break;
      }
    }
  }

  static void doWhileSwitch() {
    int i = 1;
    LOOP:
    while (true) {
      switch (i) {
        case 11:
          break LOOP;
        case 1:
        case 2:
        default:
          System.out.println(i++);
      }
    }
  }

  public static void main(String[] args) {
    whileLoop();
  }
}
