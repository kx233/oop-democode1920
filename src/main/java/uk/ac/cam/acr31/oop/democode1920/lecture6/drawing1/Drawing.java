package uk.ac.cam.acr31.oop.democode1920.lecture6.drawing1;

import java.util.List;

public class Drawing {

  static void draw(List<Circle> circles, List<Square> squares) {
    draw(circles, squares, List.of());
  }

  public static void draw(List<Circle> circles, List<Square> squares, List<Stick> sticks) {
    AsciiImage asciiImage = new AsciiImage();
    circles.forEach(c -> c.draw(asciiImage));
    squares.forEach(s -> s.draw(asciiImage));
    sticks.forEach(s -> s.draw(asciiImage));
    System.out.println(asciiImage);
  }

  public static void main(String[] args) {
    draw(List.of(new Circle(0), new Circle(5)), List.of(new Square(1), new Square(4)));
  }
}
