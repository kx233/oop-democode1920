package uk.ac.cam.acr31.oop.democode1920.lecture12;

/**
 * Model a desk fan with three settings: off, slow and fast.
 *
 * <p>The fan has one button to click through these three settings.
 *
 * <p>The two methods on this class are called by the firmware of the fan. {@code click} is called
 * whenever someone presses the button to change setting. {@code update} is called whenever the
 * motor is ready to change speed.
 */
public class FanSpeedWithInts {

  private static final int OFF = 0;
  private static final int SLOW = 1;
  private static final int FAST = 2;

  private int state = OFF;

  /** Set the motor turning at the correct speed. */
  void update(MotorController motorController) {
    switch (state) {
      case OFF:
        motorController.stop();
        return;
      case SLOW:
        motorController.turnSlow();
        return;
      case FAST:
        motorController.turnFast();
    }
  }

  /** Respond to a button click to change between settings. */
  void click() {
    switch (state) {
      case OFF:
        state = SLOW;
        return;
      case SLOW:
        state = FAST;
        return;
      case FAST:
        state = OFF;
    }
  }
}
