package uk.ac.cam.acr31.oop.democode1920.lecture7;

class ConstructedObject extends SuperObject {

  private static final String CONSTANT = print("CONSTANT");

  static {
    print("STATIC INITIALIZER");
  }

  private final String instance = print("instance");

  ConstructedObject() {
    super(print("hello"));
    print("CONSTRUCTOR");
  }

  private static final String CONSTANT2 = print("CONSTANT2");

  private final String instance2 = print("instance2");

  static String print(String message) {
    System.out.println("ConstructedObject." + message);
    return null;
  }
}
