package uk.ac.cam.acr31.oop.democode1920.lecture4;

public class ClassReferences {

  private static int a(A a) {
    return a.x + 4;
  }

  private static void b(A a) {
    a.x += 4;
  }

  public static void main(String[] args) {
    A object = new A(4);
    int x = a(object);
    b(object);
    System.out.printf("%d, %d%n", x, object.x);
  }
}
