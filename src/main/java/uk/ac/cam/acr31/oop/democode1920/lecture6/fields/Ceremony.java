package uk.ac.cam.acr31.oop.democode1920.lecture6.fields;

public class Ceremony {
  String speech;
  boolean bow;

  boolean bow() {
    return bow;
  }

  static Ceremony casual() {
    Ceremony c = new Ceremony();
    c.speech = "Hi!";
    c.bow = false;
    return c;
  }
}
