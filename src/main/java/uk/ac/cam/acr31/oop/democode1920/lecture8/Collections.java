package uk.ac.cam.acr31.oop.democode1920.lecture8;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.PriorityQueue;
import java.util.TreeMap;
import java.util.TreeSet;

public class Collections {

  private static void sets() {
    HashSet<String> set1 = new HashSet<>();
    set1.add("a1");
    set1.add("c2");
    set1.add("c2");
    set1.add("c2");
    set1.add("b3");
    set1.add("d4");
    set1.add("e5");
    System.out.println(set1);
  }

  private static void sets2() {
    TreeSet<String> set1 = new TreeSet<>();
    set1.add("a1");
    set1.add("c2");
    set1.add("c2");
    set1.add("c2");
    set1.add("b3");
    set1.add("d4");
    set1.add("e5");
    System.out.println(set1);
    System.out.println(set1.contains("b3"));
    System.out.println(set1.contains("b4"));
  }

  private static void sets3() {
    LinkedHashSet<String> set1 = new LinkedHashSet<>();
    set1.add("a1");
    set1.add("c2");
    set1.add("c2");
    set1.add("c2");
    set1.add("b3");
    set1.add("d4");
    set1.add("e5");
    System.out.println(set1);
    System.out.println(set1.contains("b3"));
    System.out.println(set1.contains("b4"));
  }

  private static void list() {
    ArrayList<String> list = new ArrayList<>();
    list.add("a1");
    list.add("c2");
    list.add("c2");
    list.add("c2");
    list.add("b3");
    list.add("d4");
    list.add("e5");
    System.out.println(list);
    list.get(2);
    list.remove(0);
  }

  private static void queue() {
    PriorityQueue<String> queue = new PriorityQueue<>();
    queue.offer("b");
    queue.offer("a");
    System.out.println(queue.poll());
    System.out.println(queue.poll());
  }

  private static void map() {
    HashMap<String, Integer> map = new HashMap<>();
    map.put("a1sadsaklm", 1);
    map.put("b2skslm23", 2);
    map.put("c3caWS", 2);
    map.put("c3caWS", 1);
    map.put("d4BGHTSS", 1);
    map.put("e5as122", 1);
    System.out.println(map);
  }

  private static void map2() {
    TreeMap<String, Integer> map = new TreeMap<>();
    map.put("c3caWS", 1);
    map.put("d4BGHTSS", 1);
    map.put("a1sadsaklm", 1);
    map.put("b2skslm23", 2);
    map.put("c3caWS", 2);
    map.put("e5as122", 1);
    map.put("not null", null);
    System.out.println(map);
    map.get("c3caWS"); // 2
    map.get("not here"); // null
    if (map.containsKey("not here")) {
      System.out.println("The key was there");
    }
  }

  public static void main(String[] args) {
    sets();
    sets2();
    sets3();
    list();
    queue();
    map();
    map2();
  }
}
