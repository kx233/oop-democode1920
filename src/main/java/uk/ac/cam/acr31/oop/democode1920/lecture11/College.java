package uk.ac.cam.acr31.oop.democode1920.lecture11;

import java.util.List;

class College {
  private final String name;
  private final int foundingYear;

  private College(String name, int foundingYear) {
    this.name = name;
    this.foundingYear = foundingYear;
  }

  String getName() {
    return name;
  }

  int getFoundingYear() {
    return foundingYear;
  }

  static List<College> colleges() {
    return List.of(
        new College("Christ's", 1505),
        new College("Churchill", 1960),
        new College("Clare", 1326),
        new College("Clare Hall", 1966),
        new College("Corpus Christi", 1352),
        new College("Darwin", 1964),
        new College("Downing", 1800),
        new College("Emmanuel", 1584),
        new College("Fitzwilliam", 1966),
        new College("Girton", 1869),
        new College("Gonville and Caius", 1348),
        new College("Homerton", 1976),
        new College("Hughes Hall", 1949),
        new College("Jesus", 1496),
        new College("King's", 1441),
        new College("Lucy Cavendish", 1965),
        new College("Magdalene", 1428),
        new College("Murray Edwards", 1954),
        new College("Newnham", 1871),
        new College("Pembroke", 1347),
        new College("Peterhouse", 1284),
        new College("Queens'", 1448),
        new College("Robinson", 1977),
        new College("St Catharine's", 1473),
        new College("St Edmund's", 1896),
        new College("St John's", 1511),
        new College("Selwyn", 1882),
        new College("Sidney Sussex", 1596),
        new College("Trinity", 1546),
        new College("Trinity Hall", 1350),
        new College("Wolfson", 1965));
  }
}
