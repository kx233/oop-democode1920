package uk.ac.cam.acr31.oop.democode1920.lecture8;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class ReturnType {

  private static SortedSet<Integer> collect(Iterable<Integer> i) {
    TreeSet<Integer> treeSet = new TreeSet<>();
    for (Integer value : i) {
      treeSet.add(value);
    }
    return treeSet;
  }

  public static void main(String[] args) {

    ArrayList<Integer> list = new ArrayList(List.of(4, 2, 1, 1, 1, 6));
    System.out.println(collect(list));

    Set<Integer> integerSet = new HashSet<>();
    integerSet.add(1);
    integerSet.add(3);
    System.out.println(collect(integerSet));
  }
}
