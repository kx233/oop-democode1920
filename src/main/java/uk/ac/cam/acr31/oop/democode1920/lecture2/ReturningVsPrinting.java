package uk.ac.cam.acr31.oop.democode1920.lecture2;

public class ReturningVsPrinting {

  static int returnsOne() {
    return 1;
  }

  public static void main(String[] args) {

    int x = returnsOne();
    int y = (x = 2);

    System.out.println(x);
    System.out.println(y);
  }
}
