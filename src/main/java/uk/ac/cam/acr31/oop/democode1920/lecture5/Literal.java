package uk.ac.cam.acr31.oop.democode1920.lecture5;

public class Literal extends SuperExpression {

  private final int value;

  public Literal(int value) {
    this.value = value;
  }

  @Override
  int evaluate() {
    return value;
  }

  @Override
  public String toString() {
    return value+"";
  }
}
