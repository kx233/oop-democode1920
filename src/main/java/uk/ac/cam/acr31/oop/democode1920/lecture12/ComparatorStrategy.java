package uk.ac.cam.acr31.oop.democode1920.lecture12;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

public class ComparatorStrategy {

  public static void main(String[] args) {
    List<Integer> values = Arrays.asList(3, 5, 7, 2, 1, 1, 2, 7, 8, 8, 9);
    Comparator<Integer> strategy =
        Comparator.<Integer>comparingInt(x -> x % 2).thenComparing(Function.identity()).reversed();
    Collections.sort(values, strategy);
    System.out.println(values);

    removeDuplicates(List.of("a", "A", "b"), String::equalsIgnoreCase);
    removeDuplicates(List.of("a", "A", "b"), String::equals);
  }

  interface EqualsTest {
    boolean isEqual(String s, String t);
  }

  private static List<String> removeDuplicates(List<String> input, EqualsTest test) {
    ArrayList<String> result = new ArrayList<>();
    for (String s : input) {
      boolean found = false;
      for (String r : result) {
        if (test.isEqual(s, r)) {
          found = true;
        }
      }
      if (!found) {
        result.add(s);
      }
    }
    return result;
  }

  private static List<String> removeDuplicatesIgnoringCase(List<String> input) {
    ArrayList<String> result = new ArrayList<>();
    for (String s : input) {
      boolean found = false;
      for (String r : result) {
        if (s.equalsIgnoreCase(r)) {
          found = true;
        }
      }
      if (!found) {
        result.add(s);
      }
    }
    return result;
  }

  private static List<String> removeDuplicatesExactMatch(List<String> input) {
    ArrayList<String> result = new ArrayList<>();
    for (String s : input) {
      boolean found = false;
      for (String r : result) {
        if (s.equals(r)) {
          found = true;
        }
      }
      if (!found) {
        result.add(s);
      }
    }
    return result;
  }
}
